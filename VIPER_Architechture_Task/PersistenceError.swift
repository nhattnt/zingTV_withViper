//
//  PersistenceError.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 08/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import Foundation

enum PersistenceError: Error{
    case managedObjectContextNotFound
    case couldNotSaveObject
    case objectNotFound
}
