//
//  VideoDetailWireFrame.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 12/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit

class VideoDetailWireFrame: VideoDetailWireFrameProtocol {
    
    class func createVideoDetailModule(forVideo video: VideoModel) -> UIViewController {
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "VideoDetailController")
        if let view = viewController as? VideoDetailView {
            let presenter: VideoDetailPresenterProtocol = VideoDetailPresenter()
            let wireFrame: VideoDetailWireFrameProtocol = VideoDetailWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.video = video
            presenter.wireFrame = wireFrame
            
            return viewController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
}
