//
//  VIPER_Architechture_TaskTests.swift
//  VIPER_Architechture_TaskTests
//
//  Created by ArrowTN on 18/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import XCTest
@testable import VIPER_Architechture_Task

class VIPER_Architechture_TaskTests: XCTestCase {
    var interactor: VideosListInteractor!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
//    class VideosInteractorOutputSpy: VideosListInteractorOutputProtocol
//    {
//        var presentVideosListCalled = false
//        
//        func didRetrieveVideos(_ videos: [VideoModel]) {
//            presentVideosListCalled = true
//        }
//        
//        func onError() {
//            presentVideosListCalled = false
//        }
//    }
//    
//    func testFormatExpirationDateShouldAskPresenterToFormatExpirationDate()
//    {
//        let videosListInteractor = VideosListInteractor()
//        
//        // Given
//        let videosListInteractorOutputSpy = VideosInteractorOutputSpy()
//        videosListInteractor.presenter = videosListInteractorOutputSpy
//        
//        // When
//        
//        
//        // Then
//        XCTAssert(videosListInteractorOutputSpy.presentVideosListCalled, "Formatting an expiration date should ask presenter to do it")
//    }
    
    func testGetDataFromJSONFile() {
        let remoteDataManager: VideosListRemoteDataManagerInputProtocol = VideosListRemoteDataManager()
        let videoList = remoteDataManager.retrieveVideosListDirectly()
        //Kiểm tra số lượng bộ thông tin về video chứa trong file json
        XCTAssert(videoList.count == 20)
        
        var testVideo = VideoModel()
        testVideo.name = "Năm Ấy Hoa Nở Trăng Vừa Tròn"
        testVideo.videoUrl =  "http://tv.zing.vn/video/Nam-Ay-Hoa-No-Trang-Vua-Tron-Tap-9/IWZDF7AW.html#home_video_box_1"
        testVideo.eps = "Tập 9"
        testVideo.imgUrl = "http://zingtv-photo-td.zadn.vn/tv_media_225_126/b/3/b3d65e0ec1c9439e100711cbe210ec09_1504605382.jpg"
        testVideo.des = "Bộ phim xoay quanh Chu Oánh vốn từ một thiếu nữ giang hồ, được gả cho đại thiếu gia Ngô Sính trở thành một phu nhân giàu có, vô tình bị rơi vào ván cờ lợi lộc. Vướng vào tình cảm rắc rối phức tạp cùng với mâu thuẫn của gia tộc. Trải qua nhiều biến cố thăng trầm, Chu Oánh từng bước trở thành nữ thương gia giàu có thành đạt nhất thời bấy giờ."
        testVideo.type = "Phim tình cảm"
        testVideo.schedule = "Thứ 2 và Thứ 3"
        
        var isHaveVideoInfoTest = false
        for video in videoList {
            if( video.name == testVideo.name && video.imgUrl == testVideo.imgUrl && video.videoUrl == testVideo.videoUrl && video.eps == testVideo.eps && video.des == testVideo.des && video.type == testVideo.type && video.schedule == testVideo.schedule){
                isHaveVideoInfoTest = true
            }
        }
        XCTAssert(isHaveVideoInfoTest)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
